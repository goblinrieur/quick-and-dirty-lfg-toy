# quick and dirty lfg toy

Here is a quick and dirty low frequency signal generator, made for fun and using only items I had in my own stock.

## Goal 

Be a generator for free using only items from stock.

Have on one frequency a triangular, square and sinus signals, have a separated and different frequency on a second squared signal.

That second output can use either is own trigger or an inputted trigger (_that can be a signal from the other frequency_).

## pics

![1](./1.png)

![2](./2.png)

![3](./3.png)

![4](./4.png)

## Kicad 

Using kicad 8 for design
